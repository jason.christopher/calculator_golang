package main

type count struct {
	number1, number2 float64
}

func (a count) multCount() float64 {
	return float64(a.number1) * (a.number2)
}
func (a count) divCount() float64 {
	return float64(a.number1) / (a.number2)
}
func (a *count) addCount() float64 {
	return float64(a.number1) + (a.number2)
}
func (a count) subCount() float64 {
	return float64(a.number1) - (a.number2)
}
