package main

import "math"

type TrigonometriCount struct {
	number1 float64
}

func (t TrigonometriCount) sinCount() float64 {
	return float64(math.Sin(t.number1 * (math.Pi / 180)))
}
func (t TrigonometriCount) cosCount() float64 {
	return float64(math.Cos(t.number1 * (math.Pi / 180)))
}
func (t TrigonometriCount) tanCount() float64 {
	return float64(math.Tan(t.number1 * (math.Pi / 180)))
}
