package main

import (
	"fmt"
	"os"
)



func main() {
	var number1, number2 float64
	Choice := 0
	var Operator string
	for {
		fmt.Println("Choose 1.Basic Calculator 2.Trigonometri 3.EXIT : ")
		fmt.Scan(&Choice)

		if Choice == 1 {
			fmt.Println("Input first number")
			fmt.Scan(&number1)
			fmt.Println("Choose operator [ * , / , + , - ]: ")
			fmt.Scan(&Operator)
			fmt.Println("Input second number")
			fmt.Scan(&number2)
			result := count{number1: number1, number2: number2}

			switch Operator {
			case "*":

				fmt.Println("Result : ", result.multCount())
				break
			case "/":

				fmt.Println("Result : ", result.divCount())
				break
			case "+":

				fmt.Println("Result : ", result.addCount())
				break
			case "-":

				fmt.Println("Result : ", result.subCount())
				break
			}
		} else if Choice == 2 {
			fmt.Println("input number : ")
			fmt.Scan(&number1)
			result := TrigonometriCount{number1: number1}
			fmt.Println("Sin : ", result.sinCount())
			fmt.Println("Cos : ", result.cosCount())
			fmt.Println("Tan : ", result.tanCount())
		} else {
			os.Exit(3)
		}

	}

}
